module diplomaProject

go 1.16

require (
	github.com/GeertJohan/go.rice v1.0.2
	github.com/aws/aws-sdk-go v1.38.28
	github.com/daaku/go.zipexe v1.0.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.1-0.20200107013213-dc14462fd587+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/mux v1.8.1-0.20200912192056-d07530f46e1e
	github.com/jinzhu/gorm v1.9.17-0.20200921022817-466b344ff592
	github.com/jinzhu/inflection v1.0.1-0.20210111022912-b5281034e75e // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/joho/godotenv v1.3.1-0.20210304093531-ddf83eb33bbb
	github.com/lib/pq v1.10.1-0.20210322173541-b2901c7946b6 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
