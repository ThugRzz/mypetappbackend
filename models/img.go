package models

type Imgpath struct {
	ID        string `json:"id"`
	ImagePath string `json:"imgpath"`
}
